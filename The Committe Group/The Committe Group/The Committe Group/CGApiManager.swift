//
//  CGApiManager.swift
//  The Committe Group
//
//  Created by Sanchan on 17/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class CGApiManager
    
{
    static let sharedManager:CGApiManager = CGApiManager()
    func postDataWithJson(url:String,parameters:[String:[String:AnyObject]],completion:@escaping (_ responseDict:Any?,_ error:Error?,_ isDone:Bool)->Void)
    {
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { response in
            do
            {
                let post:Any = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                completion(post,nil,true)
            }
            catch
            {
                completion(nil,response.result.error!,false)
            }
        }
    }
    
}
